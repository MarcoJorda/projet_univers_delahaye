import unittest 
import csv
from fonctions_dir.fonctions import *

class TestUnivers(unittest.TestCase):

        def the_most_wonderful_test(self):    

            firstname = "Marco"
            lastname = "Jorda"

            start_universe('univers_marvel') #Starting the wanted universe

            self.assertEqual("Nick Fury", get_new_firstname(firstname)) 
            self.assertEqual("La Brindille", get_new_lastname(lastname))
