"""


This projet was made by Marco Jorda(G1).


It consists in choosing a universe with which the program will give you your universe firstname
and your universe lastname after typing them on entries. 
The program manages when the entries are empty and even when you only type numbers or special
symbol, but he detects an error in the index list when you combine both. 
But still, this program was fun to make.
Feel free to add some universes and have some fun with them. 

     
"""


from tkinter import *
from tkinter.messagebox import showinfo, showwarning
from fonctions_dir.fonctions import *

height_window = 500 
width_window = 200

def get_entry_content(name_content):
    return name_content.get()
    
def check_entry_content(firstname_entry,lastname_entry):

    """
    This function checks the entries content. 
    
    """
    firstname = get_entry_content(firstname_entry)
    lastname = get_entry_content(lastname_entry)

    if len(firstname)==0 or len(lastname)==0: 
        showwarning(title="Erreur!", 
                    message="Hey ! Au moins une de ces belles cases est vide...")
        return False
    elif firstname.upper().isupper()!=True and lastname.upper().isupper()!=True :
        showwarning(title="Erreur!",    
                    message="Hey ! Il faut des chaînes de caractères dans les cases...")
        return False
    return True



def set_new_name():
    """
    This function sets you new firstname and your new lastname only if the check_entry_content
    returns True. 
    """
    if check_entry_content(firstname_entry,lastname_entry):

        firstname = get_entry_content(firstname_entry)
        lastname = get_entry_content(lastname_entry)

        universe_firstname = get_new_firstname(firstname)
        universe_lastname = get_new_lastname(lastname)

        
        showinfo(title="Suspens..", 
                 message= firstname + " " + lastname + " devient : \n"+ universe_firstname +" "+ universe_lastname+".")                 
        


def open_name_window():
    """
    This function opens name_window and hides the universe_selection_window.
    Moreover, it starts the selected universe before closing the selection_window. 
    """


    #Set start_unvierse with marvel universe as default to avoid other checks.
    start_universe("univers_marvel")
    
    universe_selection_window.withdraw()
    #universe_selection_window.destroy()
    universe_name_window.deiconify()


def center_window(window):
    """
    No need to explain this window
    """
    screen_width = window.winfo_screenwidth()
    screen_height = window.winfo_screenheight()

    x = ((float(screen_width))/2) - (width_window + 0.5*width_window)
    y = ((float(screen_height))/2) - (height_window / 3)

    window.geometry("%dx%d+%d+%d" % (height_window,width_window,x,y))


if __name__ == "__main__":
    
    
    #-------------- Première Fenêtre // Sélection de l'univers----------------------------
    
    """
    This window allows the user to select a universe.
    The selected universe will be started asa the user clicks on the validation_button
    """

    universe_selection_window = Tk()
    universe_selection_window.title("Sélection de l'univers")
    universe_selection_window.minsize(height_window,width_window)    
    universe_selection_window.maxsize(height_window,width_window)   
    center_window(universe_selection_window)


    label_espace = Label(universe_selection_window,text="")
    label_espace.pack()


    label_titre = Label(universe_selection_window, text="Sélectionnez l'univers souhaité!")
    label_titre.config(font=("Monospace", 13))
    label_titre.pack()
    

    label_espace = Label(universe_selection_window,text="")
    label_espace.pack()


    rb_var_universe = IntVar()
    rb_var_universe.set(1)

    Radiobutton(universe_selection_window, text="Univers Marvel", 
                                           variable=rb_var_universe, 
                                           command=lambda: start_universe("univers_marvel"), 
                                           value=1).pack()
    Radiobutton(universe_selection_window, text="Univers Seigneur des Anneaux / Hobbit",
                                           variable=rb_var_universe, 
                                           command=lambda: start_universe("univers_seigneur_des_anneaux"), 
                                           value=2).pack()
    Radiobutton(universe_selection_window, text="Univers Disney", 
                                           variable=rb_var_universe,
                                           command=lambda: start_universe("univers_disney"), 
                                           value=3).pack()


    label_espace = Label(universe_selection_window,text="")
    label_espace.pack()
    
    button_validation = Button(universe_selection_window, text="Entrée dans l'univers...", 
                                                          font="Monospace", 
                                                          command=open_name_window)
    button_validation.pack()



    #-------------- Deuxième Fenêtre // Saisie des nom et prénom ----------------------------

    """
    In this window the user can type his firstname and his lastname.
    After clicking in the get_your_name_button, the window will display your new firstname and lastname
    in a messagebox.showinfo 
    """

    universe_name_window = Tk()
    universe_name_window.title("Obtention du nouveau nom !")
    universe_name_window.minsize(height_window,width_window)    
    universe_name_window.maxsize(height_window,width_window)   
    center_window(universe_name_window)
    universe_name_window.withdraw()

    universe_name_window.rowconfigure(0, weight=1)
    universe_name_window.rowconfigure(1, weight=1)
    universe_name_window.columnconfigure(0, weight=1)
    universe_name_window.columnconfigure(1, weight=1)

    label_firstname = Label(universe_name_window, text="Prénom :", 
                                                  font='Monospace').grid(row = 0,
                                                                         column=0, 
                                                                         sticky='N', 
                                                                         pady=6)

    label_lastname = Label(universe_name_window,font='Monospace', 
                                                text="Nom :").grid(row=0,
                                                                   column=1, 
                                                                   sticky='N', 
                                                                   pady=6)

    lastname = StringVar() 
    #lastname.set("texte par défaut")
    lastname_entry = Entry(universe_name_window, textvariable=lastname, 
                                                    font='Monospace', 
                                                    width=20)
    lastname_entry.grid(row = 0, 
                        column = 1, 
                        sticky='new', 
                        pady=35, 
                        padx=20)


    firstname = StringVar() 
    firstname_entry = Entry(universe_name_window, textvariable=firstname, 
                                                    font='Monospace', 
                                                    width=20)
    firstname_entry.grid(row = 0, 
                        column = 0, 
                        sticky='new', 
                        pady=35, 
                        padx=20)


    get_your_name_button = Button(universe_name_window, text="Obtenir son nouveau nom",
                                                        font="Monospace",
                                                        command=set_new_name)
    get_your_name_button.grid(column=0,
                                row=1, 
                                sticky='wens', 
                                columnspan=2,
                                pady=20, 
                                padx=20)

    universe_name_window.mainloop()
    universe_selection_window.mainloop()





