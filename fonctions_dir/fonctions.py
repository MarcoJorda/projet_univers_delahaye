import csv

universe_dict = [] #universe_dict est déclaré dans l'intégralité de fonctions.py
ordered_dict = [] #idem

def start_universe(universe):

    with open(universe + '.csv', 'r') as csvin:
        #Delimiteur par défaut a ','
        reader = csv.DictReader(csvin) 
        for value in reader : 
            universe_dict.append(value)
        #print (universe_dict)

    letter = 'A'

    for value in universe_dict :  
        ordered_value={
            letter + '1' : value.get('1ere_letttre_Prenom'),
            letter + '2': value.get('1ere_letttre_Nom'),
        }
        ordered_dict.append(ordered_value)
        letter = chr(ord(letter)+1)


def get_new_firstname(firstname):
    index_firstname = ord(firstname[0]) - 65
    firstname_universe = ordered_dict[index_firstname].get(firstname[0]+'1')
    return firstname_universe

def get_new_lastname(lastname):

    index_lastname = ord(lastname[0]) - 65
    lastname_universe = ordered_dict[index_lastname].get(lastname[0]+'2')
    return lastname_universe

#print(universe_dict)
#print(ordered_dict)